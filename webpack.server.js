// import path from 'path';

const path = require('path');
module.exports = {
  // Inform webpack that we are building a bundle
  // for nodeJs, rather than for the browser
  target: 'node',

  //Tell webpack the root file of our server app

  entry: './src/index.js',

  //Tell webpack where to put the output file that is generated

  output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'build')
  },

  //Tell webpack to run babel on every file it runs through
  module : {
      rules: [
          {
              test: /\.js?$/, // only js files
              loader: 'babel-loader',
              exclude: /node_modules/,
              options : {         //options for babel-loader
                  presets: [
                      'react',    // for transpiling jsx to es5
                      'stage-0',   //for handling async code
                      ['env', {targets: {browsers: ['last 2 versions']}}] //
                  ]
              }
          }
      ]
  }
};
